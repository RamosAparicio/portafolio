import React from "react"
import Home from "../components/templates/Home"

const IndexPage = () => <Home />

export default IndexPage
