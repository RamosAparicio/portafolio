import React from "react"
import { Link } from "gatsby"
import Layout from "../layout"
import Hobbies from "../organisms/Hobbies"
import LineTime from "../organisms/LineTime"
import SEO from "../seo"
import AboutMe from "../organisms/AboutMe"

const Home = props => {
  return (
    <Layout>
      <SEO title="Home" />
      <AboutMe />
      <LineTime />
      <Hobbies />
    </Layout>
  )
}

export default Home
