import React from "react"

const LinkFooter = ({ title, links }) => {
  return (
    <div className="lg:w-1/4 md:w-1/2 w-full px-4">
      <h2 className="title-font font-medium text-white tracking-widest text-sm mb-3">
        {title}
      </h2>
      <nav className="list-none mb-10">
        {links.map(link => (
          <li>
            <a href={link.ref} className="text-gray-400 hover:text-white">
              {link.name}
            </a>
          </li>
        ))}
      </nav>
    </div>
  )
}

export default LinkFooter
