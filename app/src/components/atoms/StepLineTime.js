import React from "react"

const StepLineTime = ({
  title,
  content,
  children,
  href,
  isLastElement = false,
}) => {
  return (
    <div className="flex relative pb-12">
      {!isLastElement ? (
        <div className="h-full w-10 absolute inset-0 flex items-center justify-center">
          <div className="h-full w-1 bg-gray-800 pointer-events-none" />
        </div>
      ) : (
        ""
      )}

      <div className="flex-shrink-0 w-10 h-10 rounded-full bg-blue-500 inline-flex items-center justify-center text-white relative z-10">
        {children}
      </div>
      <div className="flex-grow pl-4">
        <a
          href={href}
          target="__blank"
          className="font-large title-font text-sm text-white mb-1 tracking-wider"
        >
          {title}
        </a>
        <p className="leading-relaxed">{content}</p>
      </div>
    </div>
  )
}

export default StepLineTime
