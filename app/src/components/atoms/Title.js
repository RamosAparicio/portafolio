import React from "react"

const Title = ({ title, titleEnd, text = "" }) => {
  let styleText = "text-2xl text-white font-bold md:text-3xl "
  styleText += text || ""

  return (
    <h2 className={styleText}>
      {" "}
      {title}
      <span className="text-indigo-600 "> {titleEnd} </span>
    </h2>
  )
}

export default Title
