import React from "react"

const LinkToGithub = props => {
  return (
    <a
      className="text-indigo-600 dark:text-indigo-400 font-bold"
      target="_blank"
      href="https://github.com/GiancarloAparicio"
    >
      @Aparicio
    </a>
  )
}

export default LinkToGithub
