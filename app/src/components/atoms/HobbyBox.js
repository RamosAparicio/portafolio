import React from "react"

const HobbyBox = ({ title, content, children }) => {
  return (
    <div className="p-4 md:w-1/3 flex flex-col text-center items-center">
      <div className="w-20 h-20 inline-flex items-center justify-center rounded-full bg-gray-800 text-green-400 mb-5 flex-shrink-0">
        {children}
      </div>
      <div className="flex-grow">
        <h2 className="text-white text-lg title-font font-medium mb-3">
          {title}
        </h2>
        <p className="leading-relaxed text-base">{content}</p>
      </div>
    </div>
  )
}

export default HobbyBox
