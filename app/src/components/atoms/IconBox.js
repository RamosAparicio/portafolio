import React from "react"

const IconBox = props => {
  return (
    <div className="p-4 md:w-1/4 sm:w-1/2 w-full">
      <div className="border-2 border-gray-800 px-4 py-6 rounded-lg">
        {props.children}
        <h2 className="title-font font-medium text-3xl text-white">2.7K</h2>
        <p className="leading-relaxed">{props.title}</p>
      </div>
    </div>
  )
}

export default IconBox
