import React from "react"

const Section = props => {
  return (
    <section className="text-gray-400 bg-gray-900 body-font">
      <div className="container px-5 py-2 mx-auto">{props.children}</div>
    </section>
  )
}

export default Section
