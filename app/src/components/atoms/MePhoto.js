import React from "react"

const MePhoto = props => {
  return (
    <div className="flex items-center justify-center lg:justify-end">
      <div className="max-w-lg">
        <img
          className="w-full h-64 object-cover object-center rounded-m    shadow"
          src="https://dummyimage.com/500x300"
          alt=""
        />
      </div>
    </div>
  )
}

export default MePhoto
