import React from "react"
import ProgramerIcon from "../atoms/icons/ProgramerIcon"
import LinkFooter from "../atoms/LinkFooter"

const Footer = props => {
  let linksAbout = [
    {
      ref: "https://github.com/GiancarloAparicio",
      name: "GitHub",
    },
    {
      ref: "https://gitlab.com/RamosAparicio",
      name: "GitLab",
    },
  ]

  let linksFollow = [
    {
      ref: "https://www.linkedin.com/in/giancarlo-ramos-aparicio-3258401b5/",
      name: "Linkedin",
    },
  ]

  return (
    <footer className="text-gray-400 bg-gray-900 body-font">
      <div className="container px-5 py-24 mx-auto flex md:items-center lg:items-start md:flex-row md:flex-nowrap flex-wrap flex-col">
        <div className="w-64 flex-shrink-0 md:mx-0 mx-auto text-center md:text-left">
          <a className="flex title-font font-medium items-center md:justify-start justify-center text-white">
            <ProgramerIcon />
            <span className="ml-3 text-xl">Software Developer</span>
          </a>
          <p className="mt-2 text-sm text-gray-500">© | Copyright reserved</p>
        </div>
        <div className="flex-grow flex flex-wrap md:pl-20 -mb-10 md:mt-0 mt-10 md:text-left text-left">
          <LinkFooter title="ABOUT" links={linksFollow} />
          <LinkFooter title="FOLLOW" links={linksAbout} />
        </div>
      </div>
    </footer>
  )
}

export default Footer
