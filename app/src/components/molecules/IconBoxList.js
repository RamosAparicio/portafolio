import React from "react"
import IconBox from "../atoms/IconBox"
import Downloads from "../atoms/icons/Downloads"
import Users from "../atoms/icons/Users"
import Music from "../atoms/icons/Music"
import Places from "../atoms/icons/Places"

const IconBoxList = props => {
  return (
    <div className="flex flex-wrap -m-4 text-center pb-20">
      <IconBox title="Downloads">
        <Downloads />
      </IconBox>

      <IconBox title="Users">
        <Users />
      </IconBox>

      <IconBox title="Files">
        <Music />
      </IconBox>

      <IconBox title="Places">
        <Places />
      </IconBox>
    </div>
  )
}

export default IconBoxList
