import { graphql, useStaticQuery } from "gatsby"
import React, { useState } from "react"
import Img from "gatsby-image"
import "../../styles/images-projects.scss"

const ImageProject = () => {
  let data = useStaticQuery(graphql`
    {
      allImageSharp {
        edges {
          node {
            id
            fluid(maxHeight: 500) {
              originalName
              ...GatsbyImageSharpFluid
            }
          }
        }
      }
    }
  `)

  let images = data.allImageSharp.edges

  return (
    <div className="container-images overflow-hidden mx-auto lg:w-3/5 md:w-1/2 md:mt-0 mt-12">
      <div className="slider">
        {images.map(edge => (
          <div className="slider__project">
            <Img
              key={edge.node.id}
              fluid={edge.node.fluid}
              className="slider__project__image"
            />
          </div>
        ))}
      </div>
    </div>
  )
}

export default ImageProject
