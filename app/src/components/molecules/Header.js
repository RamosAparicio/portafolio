import { Link } from "gatsby"
import PropTypes from "prop-types"
import React from "react"
import ProgramerIcon2 from "../atoms/icons/ProgramerIcon2"

const Header = ({ siteTitle }) => (
  <header className="text-gray-400 bg-gray-900 body-font">
    <div className="container mx-auto flex flex-wrap p-5 flex-col md:flex-row items-center">
      <a className="flex title-font font-medium items-center text-white mb-4 md:mb-0">
        <ProgramerIcon2 />
        <Link
          className="ml-3 text-xl"
          to="/"
          style={{
            color: `white`,
            textDecoration: `none`,
          }}
        >
          {siteTitle}
        </Link>
      </a>
      {/* <nav className="md:ml-auto flex flex-wrap items-center text-base justify-center">
        <a className="mr-5 hover:text-white">First Link</a>
        <a className="mr-5 hover:text-white">Second Link</a>
      </nav> */}
    </div>
  </header>
)

Header.propTypes = {
  siteTitle: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: ``,
}

export default Header
