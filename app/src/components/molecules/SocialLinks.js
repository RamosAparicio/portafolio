import React from "react"
import LinkdenIcon from "../atoms/icons/LinkdenIcon"
import GithubIcon from "../atoms/icons/GithubIcon"

const SocialLinks = props => {
  return (
    <div className="flex items-center -mx-2 mt-6">
      <a
        className="mx-2"
        target="__blank"
        href="https://www.linkedin.com/in/giancarlo-ramos-aparicio-3258401b5"
        aria-label="Linkden"
      >
        <LinkdenIcon />
      </a>
      <a
        className="mx-2"
        target="__blank"
        href="https://github.com/GiancarloAparicio"
        aria-label="Github"
      >
        <GithubIcon />
      </a>
    </div>
  )
}

export default SocialLinks
