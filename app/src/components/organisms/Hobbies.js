import React from "react"
import HobbyBox from "../atoms/HobbyBox"
import LineGreen from "../atoms/icons/LineGreen"
import Scissors from "../atoms/icons/Scissors"
import UserGreen from "../atoms/icons/UserGreen"
import Section from "../atoms/Section"
import Title from "../atoms/Title"

const Hobbies = props => {
  return (
    <Section>
      <div className="text-center mb-20 pt-20">
        <Title title="Hobbies" />
        <p className="text-base leading-relaxed xl:w-2/4 lg:w-3/4 mx-auto text-gray-400 text-opacity-80">
          {" "}
          Blue bottle crucifix vinyl post-ironic four dollar toast vegan
          taxidermy. Gastropub indxgo juice poutine, ramps microdosing banh mi
          pug.
        </p>
        <div className="flex mt-6 justify-center">
          <div className="w-16 h-1 rounded-full bg-green-500 inline-flex" />
        </div>
      </div>

      <div className="flex flex-wrap sm:-m-4 -mx-4 -mb-10 -mt-4 md:space-y-0 space-y-6">
        <HobbyBox
          title="Shooting start"
          content="Blue bottle crucifix vinyl post-ironic four dollar toast vegan taxidermy. Gastropub indxgo juice poutine, ramps microdosing banh mi pug VHS try-hard."
        >
          <LineGreen />
        </HobbyBox>

        <HobbyBox
          title="Shooting start"
          content="Blue bottle crucifix vinyl post-ironic four dollar toast vegan taxidermy. Gastropub indxgo juice poutine, ramps microdosing banh mi pug VHS try-hard."
        >
          <Scissors />
        </HobbyBox>

        <HobbyBox
          title="The Catalyzer"
          content="Blue bottle crucifix vinyl post-ironic four dollar toast vegan taxidermy. Gastropub indxgo juice poutine, ramps microdosing banh mi pug VHS try-hard."
        >
          <UserGreen />
        </HobbyBox>
      </div>
    </Section>
  )
}

/**
 <button className="flex mx-auto mt-16 text-white bg-green-500 border-0 py-2 px-8 focus:outline-none hover:bg-green-600 rounded text-lg">Learn more</button> 
 */

export default Hobbies
