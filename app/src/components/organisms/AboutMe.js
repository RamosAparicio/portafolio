import React from "react"
import Title from "../atoms/Title"
import LinkToGithub from "../atoms/LinkToGithub"
import Section from "../atoms/Section"
import IconBoxList from "../molecules/IconBoxList"
import MePhoto from "../atoms/MePhoto"
import SocialLinks from "../molecules/SocialLinks"

const AboutMe = props => {
  return (
    <Section>
      <div className="lg:flex items-center">
        <div className="lg:w-1/2">
          <Title title="Erick Giancarlo" titleEnd="Ramos Aparicio" />
          <p className="text-gray-500 dark:text-gray-400 lg:max-w-md mt-4">
            Hi I am software engineer
            <LinkToGithub />, passionate about learning new technologies. In my
            spare time, I like t read books or learn a new musical instrument.
          </p>
          <SocialLinks />
        </div>
        <div className="mt-8 lg:mt-0 lg:w-1/2">
          <MePhoto />
        </div>
      </div>
      <IconBoxList />
    </Section>
  )
}

export default AboutMe
