import React from "react"
import Anchor from "../atoms/icons/Anchor"
import Check from "../atoms/icons/Check"
import LineBlue from "../atoms/icons/LineBlue"
import PlaceBlue from "../atoms/icons/PlaceBlue"
import UserBlue from "../atoms/icons/UserBlue"
import Section from "../atoms/Section"
import StepLineTime from "../atoms/StepLineTime"
import Title from "../atoms/Title"
import ImageProject from "../molecules/ImageProject"

const LineTime = props => {
  return (
    <>
      <Section>
        <Title title="Projects" titleEnd="created" text="text-center" />
      </Section>

      <section className="text-gray-400 bg-gray-900 body-font">
        <div className="container px-5 py-2 mx-auto flex flex-wrap">
          <div className="flex flex-wrap w-full">
            <div className="lg:w-2/5 md:w-1/2 md:pr-10 md:py-6">
              <StepLineTime
                title="WebSocket-Node.js"
                content="Chat in real time, developed with Express, TypeScript in the Backend and Vue.js with Pug in Front."
                href="https://github.com/GiancarloAparicio/Real-Time-Node"
              >
                <PlaceBlue />
              </StepLineTime>
              <StepLineTime
                title="Covid-React.js"
                content="Graphic map in SVG to be able to follow the progress of covid-19 through graphics in each country."
                href="https://github.com/GiancarloAparicio/Covid---19"
              >
                <LineBlue />
              </StepLineTime>
              <StepLineTime
                title="PWA-Vue.js"
                content="PWA TODO-List application developed with vuetify / vue, basic application to be able to create notes and have a progress guide of each task performed."
                href="https://github.com/GiancarloAparicio/To-do-list-Vuetify-PWA"
              >
                <Check />
              </StepLineTime>
              <StepLineTime
                title="Api/File-Laravel"
                content="API developed with Laravel to upload files (.mp3, .mp4, .jpg, .png), save and order the files depending on the type of file."
                href="https://github.com/GiancarloAparicio/File-Upload-API"
              >
                <Anchor />
              </StepLineTime>

              <StepLineTime
                title="Desktop-Kotlin"
                content="Basic application for desktop developed with Kotlin, to keep track of sales in a bakery."
                href="https://github.com/GiancarloAparicio/AppKotlin"
                isLastElement={true}
              >
                <UserBlue />
              </StepLineTime>
            </div>
            <ImageProject />
          </div>
        </div>
      </section>
    </>
  )
}

export default LineTime
