# Gatsby-Tailwind

Start the NPM container

    docker-compose up -d

Run a script inside the container

    docker exec -it gatsby sh

Install dependencies and build

    npm install && npm run dev

Run Gatsby in development mode:

    gatsby develop -H 0.0.0.0
